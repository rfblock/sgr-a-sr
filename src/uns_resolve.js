import { Resolution } from '@unstoppabledomains/resolution';
import PromptSync from 'prompt-sync';
const resolution = new Resolution();

const prompt = PromptSync();
const input = prompt("Enter UNS Domain: ");

function resolveIpfsHash(domain) {
  resolution
    .ipfsHash(domain)
    .then((hash) =>
      console.log(
        `Access Website with Sagittarius A*: https://gateway.sgr-a.xyz/ipfs/${hash}`,
      ),
    )
    .catch(console.error);
}

resolveIpfsHash(input);