import Web3 from "web3";
import PromptSync from "prompt-sync";

const prompt = PromptSync();
const input = prompt("Enter ENS Domain: ");

const web3 = new Web3(Web3.givenProvider || "https://mainnet.infura.io/v3/bd83a572cce244939c6b2eb20a7ab1dd");

function resolveEnsDomain(domain) {
	web3.eth.ens.getContenthash(domain).then(res => {
		console.log(res.protocolType + ": " + res.decoded);
		// Prints out the protocol (IPFS/IPNS) and the hash id
	})
	.catch(console.error);
}

resolveEnsDomain(input);